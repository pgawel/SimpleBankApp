--
-- PostgreSQL database dump
--

-- Dumped from database version 10.4
-- Dumped by pg_dump version 10.4

-- Started on 2018-05-22 23:37:33

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 5 (class 2615 OID 16394)
-- Name: bank; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA bank;


ALTER SCHEMA bank OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 210 (class 1259 OID 16477)
-- Name: adres; Type: TABLE; Schema: bank; Owner: postgres
--

CREATE TABLE bank.adres (
    id_adres integer NOT NULL,
    id_klienta integer NOT NULL,
    ulica text NOT NULL,
    nr_lokalu integer,
    kod_pocztowy text NOT NULL,
    poczta text NOT NULL,
    miasto text NOT NULL,
    nr_domu text NOT NULL
);


ALTER TABLE bank.adres OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 16475)
-- Name: adres_id_adres_seq; Type: SEQUENCE; Schema: bank; Owner: postgres
--

CREATE SEQUENCE bank.adres_id_adres_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bank.adres_id_adres_seq OWNER TO postgres;

--
-- TOC entry 2932 (class 0 OID 0)
-- Dependencies: 209
-- Name: adres_id_adres_seq; Type: SEQUENCE OWNED BY; Schema: bank; Owner: postgres
--

ALTER SEQUENCE bank.adres_id_adres_seq OWNED BY bank.adres.id_adres;


--
-- TOC entry 206 (class 1259 OID 16441)
-- Name: klient; Type: TABLE; Schema: bank; Owner: postgres
--

CREATE TABLE bank.klient (
    id_klienta integer NOT NULL,
    imie text NOT NULL,
    nazwisko text NOT NULL,
    pesel bigint NOT NULL
);


ALTER TABLE bank.klient OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 16439)
-- Name: klient_id_klienta_seq; Type: SEQUENCE; Schema: bank; Owner: postgres
--

CREATE SEQUENCE bank.klient_id_klienta_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bank.klient_id_klienta_seq OWNER TO postgres;

--
-- TOC entry 2933 (class 0 OID 0)
-- Dependencies: 205
-- Name: klient_id_klienta_seq; Type: SEQUENCE OWNED BY; Schema: bank; Owner: postgres
--

ALTER SEQUENCE bank.klient_id_klienta_seq OWNED BY bank.klient.id_klienta;


--
-- TOC entry 216 (class 1259 OID 16537)
-- Name: operacje_rachunkowe; Type: TABLE; Schema: bank; Owner: postgres
--

CREATE TABLE bank.operacje_rachunkowe (
    id_operacji integer NOT NULL,
    id_rachunku integer NOT NULL,
    data_operacji date NOT NULL,
    id_rodzajoperacji integer NOT NULL,
    kwota_operacji numeric NOT NULL,
    rachunek_powiazany text NOT NULL,
    opis text
);


ALTER TABLE bank.operacje_rachunkowe OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 16535)
-- Name: operacje_rachunkowe_id_operacji_seq; Type: SEQUENCE; Schema: bank; Owner: postgres
--

CREATE SEQUENCE bank.operacje_rachunkowe_id_operacji_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bank.operacje_rachunkowe_id_operacji_seq OWNER TO postgres;

--
-- TOC entry 2934 (class 0 OID 0)
-- Dependencies: 215
-- Name: operacje_rachunkowe_id_operacji_seq; Type: SEQUENCE OWNED BY; Schema: bank; Owner: postgres
--

ALTER SEQUENCE bank.operacje_rachunkowe_id_operacji_seq OWNED BY bank.operacje_rachunkowe.id_operacji;


--
-- TOC entry 214 (class 1259 OID 16514)
-- Name: powiazania_kont; Type: TABLE; Schema: bank; Owner: postgres
--

CREATE TABLE bank.powiazania_kont (
    id_powiazania integer NOT NULL,
    id_klienta integer NOT NULL,
    id_rachunku integer NOT NULL,
    id_roli integer NOT NULL
);


ALTER TABLE bank.powiazania_kont OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 16512)
-- Name: powiazania_kont_id_powiazania_seq; Type: SEQUENCE; Schema: bank; Owner: postgres
--

CREATE SEQUENCE bank.powiazania_kont_id_powiazania_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bank.powiazania_kont_id_powiazania_seq OWNER TO postgres;

--
-- TOC entry 2935 (class 0 OID 0)
-- Dependencies: 213
-- Name: powiazania_kont_id_powiazania_seq; Type: SEQUENCE OWNED BY; Schema: bank; Owner: postgres
--

ALTER SEQUENCE bank.powiazania_kont_id_powiazania_seq OWNED BY bank.powiazania_kont.id_powiazania;


--
-- TOC entry 212 (class 1259 OID 16493)
-- Name: rachunek_klienta; Type: TABLE; Schema: bank; Owner: postgres
--

CREATE TABLE bank.rachunek_klienta (
    id_rachunku integer NOT NULL,
    id_rodzajproduktu integer NOT NULL,
    data_otwarcia date NOT NULL,
    id_waluty integer NOT NULL,
    data_zamkniecia date,
    nr_rachunku text NOT NULL,
    saldo numeric,
    czy_aktywny boolean NOT NULL
);


ALTER TABLE bank.rachunek_klienta OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 16491)
-- Name: rachunek_klienta_id_rachunku_seq; Type: SEQUENCE; Schema: bank; Owner: postgres
--

CREATE SEQUENCE bank.rachunek_klienta_id_rachunku_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bank.rachunek_klienta_id_rachunku_seq OWNER TO postgres;

--
-- TOC entry 2936 (class 0 OID 0)
-- Dependencies: 211
-- Name: rachunek_klienta_id_rachunku_seq; Type: SEQUENCE OWNED BY; Schema: bank; Owner: postgres
--

ALTER SEQUENCE bank.rachunek_klienta_id_rachunku_seq OWNED BY bank.rachunek_klienta.id_rachunku;


--
-- TOC entry 204 (class 1259 OID 16430)
-- Name: sl_rodzajoperacji; Type: TABLE; Schema: bank; Owner: postgres
--

CREATE TABLE bank.sl_rodzajoperacji (
    id_rodzajoperacji integer NOT NULL,
    nazwa_rodzajoperacji text NOT NULL
);


ALTER TABLE bank.sl_rodzajoperacji OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 16428)
-- Name: sl_rodzajoperacji_id_rodzajoperacji_seq; Type: SEQUENCE; Schema: bank; Owner: postgres
--

CREATE SEQUENCE bank.sl_rodzajoperacji_id_rodzajoperacji_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bank.sl_rodzajoperacji_id_rodzajoperacji_seq OWNER TO postgres;

--
-- TOC entry 2937 (class 0 OID 0)
-- Dependencies: 203
-- Name: sl_rodzajoperacji_id_rodzajoperacji_seq; Type: SEQUENCE OWNED BY; Schema: bank; Owner: postgres
--

ALTER SEQUENCE bank.sl_rodzajoperacji_id_rodzajoperacji_seq OWNED BY bank.sl_rodzajoperacji.id_rodzajoperacji;


--
-- TOC entry 202 (class 1259 OID 16419)
-- Name: sl_rodzajproduktu; Type: TABLE; Schema: bank; Owner: postgres
--

CREATE TABLE bank.sl_rodzajproduktu (
    id_rodzajproduktu integer NOT NULL,
    nazwa_rodzajproduktu text NOT NULL,
    skrot_rodzajproduktu text NOT NULL
);


ALTER TABLE bank.sl_rodzajproduktu OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 16417)
-- Name: sl_rodzajproduktu_id_rodzajproduktu_seq; Type: SEQUENCE; Schema: bank; Owner: postgres
--

CREATE SEQUENCE bank.sl_rodzajproduktu_id_rodzajproduktu_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bank.sl_rodzajproduktu_id_rodzajproduktu_seq OWNER TO postgres;

--
-- TOC entry 2938 (class 0 OID 0)
-- Dependencies: 201
-- Name: sl_rodzajproduktu_id_rodzajproduktu_seq; Type: SEQUENCE OWNED BY; Schema: bank; Owner: postgres
--

ALTER SEQUENCE bank.sl_rodzajproduktu_id_rodzajproduktu_seq OWNED BY bank.sl_rodzajproduktu.id_rodzajproduktu;


--
-- TOC entry 218 (class 1259 OID 16559)
-- Name: sl_rodzajtelefonu; Type: TABLE; Schema: bank; Owner: postgres
--

CREATE TABLE bank.sl_rodzajtelefonu (
    id_rodzajtelefonu integer NOT NULL,
    nazwa_rodzajtelefonu text NOT NULL
);


ALTER TABLE bank.sl_rodzajtelefonu OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 16557)
-- Name: sl_rodzajtelefonu_id_rodzajtelefonu_seq; Type: SEQUENCE; Schema: bank; Owner: postgres
--

CREATE SEQUENCE bank.sl_rodzajtelefonu_id_rodzajtelefonu_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bank.sl_rodzajtelefonu_id_rodzajtelefonu_seq OWNER TO postgres;

--
-- TOC entry 2939 (class 0 OID 0)
-- Dependencies: 217
-- Name: sl_rodzajtelefonu_id_rodzajtelefonu_seq; Type: SEQUENCE OWNED BY; Schema: bank; Owner: postgres
--

ALTER SEQUENCE bank.sl_rodzajtelefonu_id_rodzajtelefonu_seq OWNED BY bank.sl_rodzajtelefonu.id_rodzajtelefonu;


--
-- TOC entry 200 (class 1259 OID 16408)
-- Name: sl_rola; Type: TABLE; Schema: bank; Owner: postgres
--

CREATE TABLE bank.sl_rola (
    id_roli integer NOT NULL,
    nazwa_roli text NOT NULL
);


ALTER TABLE bank.sl_rola OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 16406)
-- Name: sl_rola_id_roli_seq; Type: SEQUENCE; Schema: bank; Owner: postgres
--

CREATE SEQUENCE bank.sl_rola_id_roli_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bank.sl_rola_id_roli_seq OWNER TO postgres;

--
-- TOC entry 2940 (class 0 OID 0)
-- Dependencies: 199
-- Name: sl_rola_id_roli_seq; Type: SEQUENCE OWNED BY; Schema: bank; Owner: postgres
--

ALTER SEQUENCE bank.sl_rola_id_roli_seq OWNED BY bank.sl_rola.id_roli;


--
-- TOC entry 198 (class 1259 OID 16397)
-- Name: sl_waluta; Type: TABLE; Schema: bank; Owner: postgres
--

CREATE TABLE bank.sl_waluta (
    id_waluty integer NOT NULL,
    nazwa_waluty text NOT NULL,
    skrot_waluty text NOT NULL
);


ALTER TABLE bank.sl_waluta OWNER TO postgres;

--
-- TOC entry 2941 (class 0 OID 0)
-- Dependencies: 198
-- Name: TABLE sl_waluta; Type: COMMENT; Schema: bank; Owner: postgres
--

COMMENT ON TABLE bank.sl_waluta IS 'Słownik walut';


--
-- TOC entry 197 (class 1259 OID 16395)
-- Name: sl_waluty_id_waluty_seq; Type: SEQUENCE; Schema: bank; Owner: postgres
--

CREATE SEQUENCE bank.sl_waluty_id_waluty_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bank.sl_waluty_id_waluty_seq OWNER TO postgres;

--
-- TOC entry 2942 (class 0 OID 0)
-- Dependencies: 197
-- Name: sl_waluty_id_waluty_seq; Type: SEQUENCE OWNED BY; Schema: bank; Owner: postgres
--

ALTER SEQUENCE bank.sl_waluty_id_waluty_seq OWNED BY bank.sl_waluta.id_waluty;


--
-- TOC entry 208 (class 1259 OID 16461)
-- Name: telefon; Type: TABLE; Schema: bank; Owner: postgres
--

CREATE TABLE bank.telefon (
    id_telefonu integer NOT NULL,
    id_klienta integer NOT NULL,
    id_rodzajtelefonu integer NOT NULL,
    nr_telefonu text NOT NULL
);


ALTER TABLE bank.telefon OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 16459)
-- Name: telefon_id_telefonu_seq; Type: SEQUENCE; Schema: bank; Owner: postgres
--

CREATE SEQUENCE bank.telefon_id_telefonu_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bank.telefon_id_telefonu_seq OWNER TO postgres;

--
-- TOC entry 2943 (class 0 OID 0)
-- Dependencies: 207
-- Name: telefon_id_telefonu_seq; Type: SEQUENCE OWNED BY; Schema: bank; Owner: postgres
--

ALTER SEQUENCE bank.telefon_id_telefonu_seq OWNED BY bank.telefon.id_telefonu;


--
-- TOC entry 2747 (class 2604 OID 16480)
-- Name: adres id_adres; Type: DEFAULT; Schema: bank; Owner: postgres
--

ALTER TABLE ONLY bank.adres ALTER COLUMN id_adres SET DEFAULT nextval('bank.adres_id_adres_seq'::regclass);


--
-- TOC entry 2745 (class 2604 OID 16444)
-- Name: klient id_klienta; Type: DEFAULT; Schema: bank; Owner: postgres
--

ALTER TABLE ONLY bank.klient ALTER COLUMN id_klienta SET DEFAULT nextval('bank.klient_id_klienta_seq'::regclass);


--
-- TOC entry 2750 (class 2604 OID 16540)
-- Name: operacje_rachunkowe id_operacji; Type: DEFAULT; Schema: bank; Owner: postgres
--

ALTER TABLE ONLY bank.operacje_rachunkowe ALTER COLUMN id_operacji SET DEFAULT nextval('bank.operacje_rachunkowe_id_operacji_seq'::regclass);


--
-- TOC entry 2749 (class 2604 OID 16517)
-- Name: powiazania_kont id_powiazania; Type: DEFAULT; Schema: bank; Owner: postgres
--

ALTER TABLE ONLY bank.powiazania_kont ALTER COLUMN id_powiazania SET DEFAULT nextval('bank.powiazania_kont_id_powiazania_seq'::regclass);


--
-- TOC entry 2748 (class 2604 OID 16496)
-- Name: rachunek_klienta id_rachunku; Type: DEFAULT; Schema: bank; Owner: postgres
--

ALTER TABLE ONLY bank.rachunek_klienta ALTER COLUMN id_rachunku SET DEFAULT nextval('bank.rachunek_klienta_id_rachunku_seq'::regclass);


--
-- TOC entry 2744 (class 2604 OID 16433)
-- Name: sl_rodzajoperacji id_rodzajoperacji; Type: DEFAULT; Schema: bank; Owner: postgres
--

ALTER TABLE ONLY bank.sl_rodzajoperacji ALTER COLUMN id_rodzajoperacji SET DEFAULT nextval('bank.sl_rodzajoperacji_id_rodzajoperacji_seq'::regclass);


--
-- TOC entry 2743 (class 2604 OID 16422)
-- Name: sl_rodzajproduktu id_rodzajproduktu; Type: DEFAULT; Schema: bank; Owner: postgres
--

ALTER TABLE ONLY bank.sl_rodzajproduktu ALTER COLUMN id_rodzajproduktu SET DEFAULT nextval('bank.sl_rodzajproduktu_id_rodzajproduktu_seq'::regclass);


--
-- TOC entry 2751 (class 2604 OID 16562)
-- Name: sl_rodzajtelefonu id_rodzajtelefonu; Type: DEFAULT; Schema: bank; Owner: postgres
--

ALTER TABLE ONLY bank.sl_rodzajtelefonu ALTER COLUMN id_rodzajtelefonu SET DEFAULT nextval('bank.sl_rodzajtelefonu_id_rodzajtelefonu_seq'::regclass);


--
-- TOC entry 2742 (class 2604 OID 16411)
-- Name: sl_rola id_roli; Type: DEFAULT; Schema: bank; Owner: postgres
--

ALTER TABLE ONLY bank.sl_rola ALTER COLUMN id_roli SET DEFAULT nextval('bank.sl_rola_id_roli_seq'::regclass);


--
-- TOC entry 2741 (class 2604 OID 16400)
-- Name: sl_waluta id_waluty; Type: DEFAULT; Schema: bank; Owner: postgres
--

ALTER TABLE ONLY bank.sl_waluta ALTER COLUMN id_waluty SET DEFAULT nextval('bank.sl_waluty_id_waluty_seq'::regclass);


--
-- TOC entry 2746 (class 2604 OID 16464)
-- Name: telefon id_telefonu; Type: DEFAULT; Schema: bank; Owner: postgres
--

ALTER TABLE ONLY bank.telefon ALTER COLUMN id_telefonu SET DEFAULT nextval('bank.telefon_id_telefonu_seq'::regclass);


--
-- TOC entry 2918 (class 0 OID 16477)
-- Dependencies: 210
-- Data for Name: adres; Type: TABLE DATA; Schema: bank; Owner: postgres
--

INSERT INTO bank.adres VALUES (16, 6, 'Kościuszki', 1, '44-100', 'Gliwice', 'Gliwice', '3B');
INSERT INTO bank.adres VALUES (3, 6, 'Chorzowska', 41, '40-001', 'Katowice', 'Katowice', '107');
INSERT INTO bank.adres VALUES (4, 7, 'Tarnogórska', NULL, '44-100', 'Gliwice', 'Gliwice', '45');
INSERT INTO bank.adres VALUES (5, 8, '3 Maja', 14, '41-800', 'Zabrze', 'Zabrze', '86');
INSERT INTO bank.adres VALUES (6, 9, 'Bosacka', 3, '31-505', 'Kraków', 'Kraków', '1');
INSERT INTO bank.adres VALUES (9, 10, 'Zwycięstwa', 4, '44-100', 'Gliwice', 'Gliwice', '36');
INSERT INTO bank.adres VALUES (10, 19, 'Wolności', 2, '44-000', 'zabrze', 'zabrze', '1');
INSERT INTO bank.adres VALUES (11, 21, 'Toruńska', 3, '44-001', 'Gliwice', 'Gliwice', '13');
INSERT INTO bank.adres VALUES (12, 22, 'Słoneczka', 10, '00-789', 'Warszawa', 'Warszawa', '7');
INSERT INTO bank.adres VALUES (1, 1, 'Wieczorka', 2, '44-100', 'Gliwice', 'Gliwice', '1');
INSERT INTO bank.adres VALUES (7, 1, 'Akademicka', 3, '44-100', 'Gliwice', 'Gliwice', '3');
INSERT INTO bank.adres VALUES (15, 25, 'Zygmunta Starego', 4, '44-100', 'Gliwice', 'Gliwice', '12');


--
-- TOC entry 2914 (class 0 OID 16441)
-- Dependencies: 206
-- Data for Name: klient; Type: TABLE DATA; Schema: bank; Owner: postgres
--

INSERT INTO bank.klient VALUES (6, 'Katarzyna', 'Kowalska', 92120112345);
INSERT INTO bank.klient VALUES (7, 'Franciszek', 'Deszcz', 58063032156);
INSERT INTO bank.klient VALUES (8, 'Roman', 'Nowakowski', 86040578954);
INSERT INTO bank.klient VALUES (9, 'Agnieszka', 'Pogodna', 94090596358);
INSERT INTO bank.klient VALUES (10, 'Piotr', 'Kowal', 96082212396);
INSERT INTO bank.klient VALUES (19, 'Jan', 'Dzban', 78965412365);
INSERT INTO bank.klient VALUES (21, 'Krzysztof', 'Krawczyk', 78052145845);
INSERT INTO bank.klient VALUES (22, 'Andrzej', 'Góral', 89103015987);
INSERT INTO bank.klient VALUES (1, 'Robert', 'Nowak', 76062306066);
INSERT INTO bank.klient VALUES (25, 'Renata', 'Długopis', 99052188965);


--
-- TOC entry 2924 (class 0 OID 16537)
-- Dependencies: 216
-- Data for Name: operacje_rachunkowe; Type: TABLE DATA; Schema: bank; Owner: postgres
--

INSERT INTO bank.operacje_rachunkowe VALUES (1, 6, '2018-05-05', 1, 26.99, '19536975208439853690125495', NULL);
INSERT INTO bank.operacje_rachunkowe VALUES (4, 5, '2018-05-15', 2, 239.90, '98519603950142085132963685', NULL);
INSERT INTO bank.operacje_rachunkowe VALUES (3, 5, '2018-05-18', 1, 300.00, '73019603950142085132964264', 'Just a transaction');
INSERT INTO bank.operacje_rachunkowe VALUES (5, 7, '2017-01-22', 1, 100.00, '89319030601054976349436975', 'Pocket money');
INSERT INTO bank.operacje_rachunkowe VALUES (9, 5, '2018-02-25', 1, 59.30, '98713684000016113501549785', 'Food');
INSERT INTO bank.operacje_rachunkowe VALUES (10, 5, '2017-12-29', 2, 78.40, '79631948734953497832649700', NULL);
INSERT INTO bank.operacje_rachunkowe VALUES (6, 9, '2018-03-01', 1, 156.98, '78965418494639463194975398', 'Groceries');


--
-- TOC entry 2922 (class 0 OID 16514)
-- Dependencies: 214
-- Data for Name: powiazania_kont; Type: TABLE DATA; Schema: bank; Owner: postgres
--

INSERT INTO bank.powiazania_kont VALUES (1, 1, 5, 2);
INSERT INTO bank.powiazania_kont VALUES (2, 1, 6, 1);
INSERT INTO bank.powiazania_kont VALUES (3, 6, 5, 2);
INSERT INTO bank.powiazania_kont VALUES (4, 7, 7, 1);
INSERT INTO bank.powiazania_kont VALUES (5, 8, 8, 1);
INSERT INTO bank.powiazania_kont VALUES (6, 9, 9, 1);


--
-- TOC entry 2920 (class 0 OID 16493)
-- Dependencies: 212
-- Data for Name: rachunek_klienta; Type: TABLE DATA; Schema: bank; Owner: postgres
--

INSERT INTO bank.rachunek_klienta VALUES (5, 2, '2018-05-01', 1, NULL, '12345698523664785123958475', 20.54, true);
INSERT INTO bank.rachunek_klienta VALUES (6, 1, '2012-04-20', 1, NULL, '78523985439648519578543958', 20495.09, true);
INSERT INTO bank.rachunek_klienta VALUES (7, 3, '2014-12-04', 3, NULL, '78543984319030940398452568', 782.44, true);
INSERT INTO bank.rachunek_klienta VALUES (9, 1, '2015-03-23', 4, NULL, '78236014850395103984241902', 1950.00, true);
INSERT INTO bank.rachunek_klienta VALUES (8, 1, '2009-07-30', 1, '2017-05-24', '78536941285639845302001405', 0.00, false);


--
-- TOC entry 2912 (class 0 OID 16430)
-- Dependencies: 204
-- Data for Name: sl_rodzajoperacji; Type: TABLE DATA; Schema: bank; Owner: postgres
--

INSERT INTO bank.sl_rodzajoperacji VALUES (1, 'przelew wewnetrzny');
INSERT INTO bank.sl_rodzajoperacji VALUES (2, 'przelew zewnetrzny');


--
-- TOC entry 2910 (class 0 OID 16419)
-- Dependencies: 202
-- Data for Name: sl_rodzajproduktu; Type: TABLE DATA; Schema: bank; Owner: postgres
--

INSERT INTO bank.sl_rodzajproduktu VALUES (1, 'Konto standardowe', 'KST');
INSERT INTO bank.sl_rodzajproduktu VALUES (2, 'Konto dla mlodych', 'KDM');
INSERT INTO bank.sl_rodzajproduktu VALUES (3, 'Konto firmowe', 'KFI');


--
-- TOC entry 2926 (class 0 OID 16559)
-- Dependencies: 218
-- Data for Name: sl_rodzajtelefonu; Type: TABLE DATA; Schema: bank; Owner: postgres
--

INSERT INTO bank.sl_rodzajtelefonu VALUES (1, 'komórkowy');
INSERT INTO bank.sl_rodzajtelefonu VALUES (2, 'stacjonarny');
INSERT INTO bank.sl_rodzajtelefonu VALUES (3, 'służbowy');


--
-- TOC entry 2908 (class 0 OID 16408)
-- Dependencies: 200
-- Data for Name: sl_rola; Type: TABLE DATA; Schema: bank; Owner: postgres
--

INSERT INTO bank.sl_rola VALUES (1, 'posiadacz');
INSERT INTO bank.sl_rola VALUES (3, 'gwarant');
INSERT INTO bank.sl_rola VALUES (2, 'współposiadacz');
INSERT INTO bank.sl_rola VALUES (4, 'poręczyciel');
INSERT INTO bank.sl_rola VALUES (5, 'pełnomocnik');


--
-- TOC entry 2906 (class 0 OID 16397)
-- Dependencies: 198
-- Data for Name: sl_waluta; Type: TABLE DATA; Schema: bank; Owner: postgres
--

INSERT INTO bank.sl_waluta VALUES (1, 'złoty', 'PLN');
INSERT INTO bank.sl_waluta VALUES (3, 'euro', 'EUR');
INSERT INTO bank.sl_waluta VALUES (4, 'dolar amerykański', 'USD');


--
-- TOC entry 2916 (class 0 OID 16461)
-- Dependencies: 208
-- Data for Name: telefon; Type: TABLE DATA; Schema: bank; Owner: postgres
--

INSERT INTO bank.telefon VALUES (2, 6, 1, '789456123');
INSERT INTO bank.telefon VALUES (3, 7, 2, '324568974');
INSERT INTO bank.telefon VALUES (4, 8, 3, '456396741');
INSERT INTO bank.telefon VALUES (5, 9, 1, '456862159');
INSERT INTO bank.telefon VALUES (8, 10, 1, '951862496');
INSERT INTO bank.telefon VALUES (9, 21, 1, '789654123');
INSERT INTO bank.telefon VALUES (10, 22, 1, '123456789');
INSERT INTO bank.telefon VALUES (1, 1, 3, '777444111');
INSERT INTO bank.telefon VALUES (11, 25, 3, '789-000-654');


--
-- TOC entry 2944 (class 0 OID 0)
-- Dependencies: 209
-- Name: adres_id_adres_seq; Type: SEQUENCE SET; Schema: bank; Owner: postgres
--

SELECT pg_catalog.setval('bank.adres_id_adres_seq', 16, true);


--
-- TOC entry 2945 (class 0 OID 0)
-- Dependencies: 205
-- Name: klient_id_klienta_seq; Type: SEQUENCE SET; Schema: bank; Owner: postgres
--

SELECT pg_catalog.setval('bank.klient_id_klienta_seq', 25, true);


--
-- TOC entry 2946 (class 0 OID 0)
-- Dependencies: 215
-- Name: operacje_rachunkowe_id_operacji_seq; Type: SEQUENCE SET; Schema: bank; Owner: postgres
--

SELECT pg_catalog.setval('bank.operacje_rachunkowe_id_operacji_seq', 10, true);


--
-- TOC entry 2947 (class 0 OID 0)
-- Dependencies: 213
-- Name: powiazania_kont_id_powiazania_seq; Type: SEQUENCE SET; Schema: bank; Owner: postgres
--

SELECT pg_catalog.setval('bank.powiazania_kont_id_powiazania_seq', 6, true);


--
-- TOC entry 2948 (class 0 OID 0)
-- Dependencies: 211
-- Name: rachunek_klienta_id_rachunku_seq; Type: SEQUENCE SET; Schema: bank; Owner: postgres
--

SELECT pg_catalog.setval('bank.rachunek_klienta_id_rachunku_seq', 8, true);


--
-- TOC entry 2949 (class 0 OID 0)
-- Dependencies: 203
-- Name: sl_rodzajoperacji_id_rodzajoperacji_seq; Type: SEQUENCE SET; Schema: bank; Owner: postgres
--

SELECT pg_catalog.setval('bank.sl_rodzajoperacji_id_rodzajoperacji_seq', 2, true);


--
-- TOC entry 2950 (class 0 OID 0)
-- Dependencies: 201
-- Name: sl_rodzajproduktu_id_rodzajproduktu_seq; Type: SEQUENCE SET; Schema: bank; Owner: postgres
--

SELECT pg_catalog.setval('bank.sl_rodzajproduktu_id_rodzajproduktu_seq', 3, true);


--
-- TOC entry 2951 (class 0 OID 0)
-- Dependencies: 217
-- Name: sl_rodzajtelefonu_id_rodzajtelefonu_seq; Type: SEQUENCE SET; Schema: bank; Owner: postgres
--

SELECT pg_catalog.setval('bank.sl_rodzajtelefonu_id_rodzajtelefonu_seq', 3, true);


--
-- TOC entry 2952 (class 0 OID 0)
-- Dependencies: 199
-- Name: sl_rola_id_roli_seq; Type: SEQUENCE SET; Schema: bank; Owner: postgres
--

SELECT pg_catalog.setval('bank.sl_rola_id_roli_seq', 5, true);


--
-- TOC entry 2953 (class 0 OID 0)
-- Dependencies: 197
-- Name: sl_waluty_id_waluty_seq; Type: SEQUENCE SET; Schema: bank; Owner: postgres
--

SELECT pg_catalog.setval('bank.sl_waluty_id_waluty_seq', 4, true);


--
-- TOC entry 2954 (class 0 OID 0)
-- Dependencies: 207
-- Name: telefon_id_telefonu_seq; Type: SEQUENCE SET; Schema: bank; Owner: postgres
--

SELECT pg_catalog.setval('bank.telefon_id_telefonu_seq', 11, true);


--
-- TOC entry 2765 (class 2606 OID 16485)
-- Name: adres adres_pkey; Type: CONSTRAINT; Schema: bank; Owner: postgres
--

ALTER TABLE ONLY bank.adres
    ADD CONSTRAINT adres_pkey PRIMARY KEY (id_adres);


--
-- TOC entry 2761 (class 2606 OID 16449)
-- Name: klient klient_pkey; Type: CONSTRAINT; Schema: bank; Owner: postgres
--

ALTER TABLE ONLY bank.klient
    ADD CONSTRAINT klient_pkey PRIMARY KEY (id_klienta);


--
-- TOC entry 2771 (class 2606 OID 16545)
-- Name: operacje_rachunkowe operacje_rachunkowe_pkey; Type: CONSTRAINT; Schema: bank; Owner: postgres
--

ALTER TABLE ONLY bank.operacje_rachunkowe
    ADD CONSTRAINT operacje_rachunkowe_pkey PRIMARY KEY (id_operacji);


--
-- TOC entry 2769 (class 2606 OID 16519)
-- Name: powiazania_kont powiazania_kont_pkey; Type: CONSTRAINT; Schema: bank; Owner: postgres
--

ALTER TABLE ONLY bank.powiazania_kont
    ADD CONSTRAINT powiazania_kont_pkey PRIMARY KEY (id_powiazania);


--
-- TOC entry 2767 (class 2606 OID 16501)
-- Name: rachunek_klienta rachunek_klienta_pkey; Type: CONSTRAINT; Schema: bank; Owner: postgres
--

ALTER TABLE ONLY bank.rachunek_klienta
    ADD CONSTRAINT rachunek_klienta_pkey PRIMARY KEY (id_rachunku);


--
-- TOC entry 2759 (class 2606 OID 16438)
-- Name: sl_rodzajoperacji sl_rodzajoperacji_pkey; Type: CONSTRAINT; Schema: bank; Owner: postgres
--

ALTER TABLE ONLY bank.sl_rodzajoperacji
    ADD CONSTRAINT sl_rodzajoperacji_pkey PRIMARY KEY (id_rodzajoperacji);


--
-- TOC entry 2757 (class 2606 OID 16427)
-- Name: sl_rodzajproduktu sl_rodzajproduktu_pkey; Type: CONSTRAINT; Schema: bank; Owner: postgres
--

ALTER TABLE ONLY bank.sl_rodzajproduktu
    ADD CONSTRAINT sl_rodzajproduktu_pkey PRIMARY KEY (id_rodzajproduktu);


--
-- TOC entry 2773 (class 2606 OID 16567)
-- Name: sl_rodzajtelefonu sl_rodzajtelefonu_pkey; Type: CONSTRAINT; Schema: bank; Owner: postgres
--

ALTER TABLE ONLY bank.sl_rodzajtelefonu
    ADD CONSTRAINT sl_rodzajtelefonu_pkey PRIMARY KEY (id_rodzajtelefonu);


--
-- TOC entry 2755 (class 2606 OID 16416)
-- Name: sl_rola sl_rola_pkey; Type: CONSTRAINT; Schema: bank; Owner: postgres
--

ALTER TABLE ONLY bank.sl_rola
    ADD CONSTRAINT sl_rola_pkey PRIMARY KEY (id_roli);


--
-- TOC entry 2753 (class 2606 OID 16405)
-- Name: sl_waluta sl_waluty_pkey; Type: CONSTRAINT; Schema: bank; Owner: postgres
--

ALTER TABLE ONLY bank.sl_waluta
    ADD CONSTRAINT sl_waluty_pkey PRIMARY KEY (id_waluty);


--
-- TOC entry 2763 (class 2606 OID 16469)
-- Name: telefon telefon_pkey; Type: CONSTRAINT; Schema: bank; Owner: postgres
--

ALTER TABLE ONLY bank.telefon
    ADD CONSTRAINT telefon_pkey PRIMARY KEY (id_telefonu);


--
-- TOC entry 2774 (class 2606 OID 16470)
-- Name: telefon id_klienta; Type: FK CONSTRAINT; Schema: bank; Owner: postgres
--

ALTER TABLE ONLY bank.telefon
    ADD CONSTRAINT id_klienta FOREIGN KEY (id_klienta) REFERENCES bank.klient(id_klienta);


--
-- TOC entry 2776 (class 2606 OID 16486)
-- Name: adres id_klienta; Type: FK CONSTRAINT; Schema: bank; Owner: postgres
--

ALTER TABLE ONLY bank.adres
    ADD CONSTRAINT id_klienta FOREIGN KEY (id_klienta) REFERENCES bank.klient(id_klienta);


--
-- TOC entry 2779 (class 2606 OID 16520)
-- Name: powiazania_kont id_klienta; Type: FK CONSTRAINT; Schema: bank; Owner: postgres
--

ALTER TABLE ONLY bank.powiazania_kont
    ADD CONSTRAINT id_klienta FOREIGN KEY (id_klienta) REFERENCES bank.klient(id_klienta);


--
-- TOC entry 2780 (class 2606 OID 16525)
-- Name: powiazania_kont id_rachunku; Type: FK CONSTRAINT; Schema: bank; Owner: postgres
--

ALTER TABLE ONLY bank.powiazania_kont
    ADD CONSTRAINT id_rachunku FOREIGN KEY (id_rachunku) REFERENCES bank.rachunek_klienta(id_rachunku);


--
-- TOC entry 2782 (class 2606 OID 16546)
-- Name: operacje_rachunkowe id_rachunku; Type: FK CONSTRAINT; Schema: bank; Owner: postgres
--

ALTER TABLE ONLY bank.operacje_rachunkowe
    ADD CONSTRAINT id_rachunku FOREIGN KEY (id_rachunku) REFERENCES bank.rachunek_klienta(id_rachunku);


--
-- TOC entry 2783 (class 2606 OID 16551)
-- Name: operacje_rachunkowe id_rodzajoperacji; Type: FK CONSTRAINT; Schema: bank; Owner: postgres
--

ALTER TABLE ONLY bank.operacje_rachunkowe
    ADD CONSTRAINT id_rodzajoperacji FOREIGN KEY (id_rodzajoperacji) REFERENCES bank.sl_rodzajoperacji(id_rodzajoperacji);


--
-- TOC entry 2777 (class 2606 OID 16502)
-- Name: rachunek_klienta id_rodzajproduktu; Type: FK CONSTRAINT; Schema: bank; Owner: postgres
--

ALTER TABLE ONLY bank.rachunek_klienta
    ADD CONSTRAINT id_rodzajproduktu FOREIGN KEY (id_rodzajproduktu) REFERENCES bank.sl_rodzajproduktu(id_rodzajproduktu);


--
-- TOC entry 2775 (class 2606 OID 16568)
-- Name: telefon id_rodzajtelefonu; Type: FK CONSTRAINT; Schema: bank; Owner: postgres
--

ALTER TABLE ONLY bank.telefon
    ADD CONSTRAINT id_rodzajtelefonu FOREIGN KEY (id_rodzajtelefonu) REFERENCES bank.sl_rodzajtelefonu(id_rodzajtelefonu);


--
-- TOC entry 2781 (class 2606 OID 16530)
-- Name: powiazania_kont id_roli; Type: FK CONSTRAINT; Schema: bank; Owner: postgres
--

ALTER TABLE ONLY bank.powiazania_kont
    ADD CONSTRAINT id_roli FOREIGN KEY (id_roli) REFERENCES bank.sl_rola(id_roli);


--
-- TOC entry 2778 (class 2606 OID 16507)
-- Name: rachunek_klienta id_waluty; Type: FK CONSTRAINT; Schema: bank; Owner: postgres
--

ALTER TABLE ONLY bank.rachunek_klienta
    ADD CONSTRAINT id_waluty FOREIGN KEY (id_waluty) REFERENCES bank.sl_waluta(id_waluty);


-- Completed on 2018-05-22 23:37:33

--
-- PostgreSQL database dump complete
--

