﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Npgsql;
using SimpleBankApp.Models;

namespace SimpleBankApp.DAO
{
    public class ClientsAccountDao
    {
        /// <summary>
        /// Get specified account from database
        /// </summary>
        /// <returns>ClientsAccountModel object</returns>
        public static ClientsAccountModel GetAccount(int accountId)
        {
            ClientsAccountModel account = new ClientsAccountModel();

            string query = @"SELECT * 
                             FROM bank.rachunek_klienta rk
                             JOIN bank.sl_rodzajproduktu ro ON rk.id_rodzajproduktu = ro.id_rodzajproduktu
                             JOIN bank.sl_waluta w ON rk.id_waluty = w.id_waluty
                             WHERE rk.id_rachunku = :AccountId";

            NpgsqlConnection connection = DatabaseConnection.GetInstance().Connection;
            using (var cmd = new NpgsqlCommand(query, connection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("AccountId", accountId));
                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                        account = new ClientsAccountModel(reader);
            }

            // get all transactions and clients for given account
            account.Transactions = TransactionDao.GetTransactionsList(accountId);
            account.Clients = ClientDao.GetClientsList(account.AccountId);
            
            return account;
        }

        /// <summary>
        /// Get accounts for specified client from database
        /// </summary>
        /// <param name="clientId">Client's id</param>
        /// <returns>List of all accounts for given client</returns>
        public static List<ClientsAccountModel> GetAccountsList(int clientId)
        {
            List<ClientsAccountModel> accounts = new List<ClientsAccountModel>();

            string query = @"SELECT * 
                             FROM bank.powiazania_kont pk
                             JOIN bank.sl_rola sr ON pk.id_roli = sr.id_roli
                             JOIN bank.rachunek_klienta rk ON rk.id_rachunku = pk.id_rachunku
                             JOIN bank.sl_rodzajproduktu ro ON rk.id_rodzajproduktu = ro.id_rodzajproduktu
                             JOIN bank.sl_waluta w ON rk.id_waluty = w.id_waluty
                             WHERE pk.id_klienta = :ClientId";

            NpgsqlConnection connection = DatabaseConnection.GetInstance().Connection;
            using (var cmd = new NpgsqlCommand(query, connection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("ClientId", clientId));
                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        ClientsAccountModel account = new ClientsAccountModel(reader);
                        account.Role = new DictRoleModel(reader);
                        accounts.Add(account);
                    }
            }

            // get all clients, phones and accounts for given client
            foreach (ClientsAccountModel account in accounts)
            {
                account.Clients = ClientDao.GetClientsList(account.AccountId);
            }

            return accounts;
        }

        /// <summary>
        /// Get all accounts from database
        /// </summary>
        /// <returns>List of all accounts in database</returns>
        public static List<ClientsAccountModel> GetAccountsList()
        {
            List<ClientsAccountModel> items = new List<ClientsAccountModel>();

            string query = @"SELECT * 
                             FROM bank.rachunek_klienta rk
                             JOIN bank.sl_rodzajproduktu ro ON rk.id_rodzajproduktu = ro.id_rodzajproduktu
                             JOIN bank.sl_waluta w ON rk.id_waluty = w.id_waluty
                             ORDER BY rk.id_rachunku";

            NpgsqlConnection connection = DatabaseConnection.GetInstance().Connection;
            using (var cmd = new NpgsqlCommand(query, connection))
            {
                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                        items.Add(new ClientsAccountModel(reader));
            }

            return items;
        }
    }
}