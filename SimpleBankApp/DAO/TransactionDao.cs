﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Npgsql;
using SimpleBankApp.Models;

namespace SimpleBankApp.DAO
{
    public class TransactionDao
    {
        /// <summary>
        /// Get all transactions for specified account from database
        /// </summary>
        /// <param name="accountId">Account's id</param>
        /// <returns>List of all transactions for given account</returns>
        public static List<TransactionModel> GetTransactionsList(int accountId)
        {
            List<TransactionModel> items = new List<TransactionModel>();

            string query = @"SELECT * 
                             FROM bank.operacje_rachunkowe opr
                             JOIN bank.sl_rodzajoperacji ro ON opr.id_rodzajoperacji = ro.id_rodzajoperacji
                             WHERE opr.id_rachunku = :AccountId
                             ORDER BY opr.data_operacji DESC";

            NpgsqlConnection connection = DatabaseConnection.GetInstance().Connection;

            // get data
            using (var cmd = new NpgsqlCommand(query, connection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("AccountId", accountId));
                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                        items.Add(new TransactionModel(reader));
            }

            return items;
        }
    }
}