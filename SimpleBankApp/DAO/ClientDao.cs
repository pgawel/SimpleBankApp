﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Npgsql;
using SimpleBankApp.Models;

namespace SimpleBankApp.DAO
{
    public class ClientDao
    {
        /// <summary>
        /// Get specified client from database
        /// </summary>
        /// <param name="clientId">Client's id</param>
        /// <returns>ClientModel object</returns>
        public static ClientModel GetClient(int clientId)
        {
            ClientModel client = new ClientModel();

            string query = @"SELECT * 
                             FROM bank.klient k
                             WHERE k.id_klienta = :ClientId";

            NpgsqlConnection connection = DatabaseConnection.GetInstance().Connection;

            // get data
            using (var cmd = new NpgsqlCommand(query, connection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("ClientId", clientId));
                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                        client = new ClientModel(reader);
            }

            // get all addresses, phones and accounts for given client
            client.Addresses = AddressDao.GetAddressesList(client.ClientId);
            client.Phones = PhoneDao.GetPhonesList(client.ClientId);
            client.Accounts = ClientsAccountDao.GetAccountsList(client.ClientId);

            return client;
        }

        /// <summary>
        /// Get clients for specified account from database
        /// </summary>
        /// <param name="accountId">Account's id</param>
        /// <returns>List of all clients for given account</returns>
        public static List<ClientModel> GetClientsList(int accountId)
        {
            List<ClientModel> clients = new List<ClientModel>();

            string query = @"SELECT * 
                             FROM bank.powiazania_kont pk
                             JOIN bank.sl_rola sr ON pk.id_roli = sr.id_roli
                             JOIN bank.klient k ON k.id_klienta = pk.id_klienta
                             WHERE pk.id_rachunku = :AccountId";

            NpgsqlConnection connection = DatabaseConnection.GetInstance().Connection;

            // get data
            using (var cmd = new NpgsqlCommand(query, connection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("AccountId", accountId));
                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        ClientModel client = new ClientModel(reader);
                        client.Role = new DictRoleModel(reader);
                        clients.Add(client);
                    }
            }

            // get all addresses, phones and accounts for given client
            foreach (ClientModel client in clients)
            {
                client.Addresses = AddressDao.GetAddressesList(client.ClientId);
                client.Phones = PhoneDao.GetPhonesList(client.ClientId);
            }

            return clients;
        }

        /// <summary>
        /// Get all clients from database
        /// </summary>
        /// <returns>List of all clients</returns>
        public static List<ClientModel> GetClientsList()
        {
            List<ClientModel> clients = new List<ClientModel>();

            string query = @"SELECT * 
                             FROM bank.klient 
                             ORDER BY id_klienta";

            NpgsqlConnection connection = DatabaseConnection.GetInstance().Connection;

            // get data
            using (var cmd = new NpgsqlCommand(query, connection))
            {
                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                        clients.Add(new ClientModel(reader));
            }

            // get all addresses, phones and accounts for given client
            foreach (ClientModel item in clients)
            {
                item.Addresses = AddressDao.GetAddressesList(item.ClientId);
                item.Phones = PhoneDao.GetPhonesList(item.ClientId);
                item.Accounts = ClientsAccountDao.GetAccountsList(item.ClientId);
            }

            return clients;
        }

        /// <summary>
        /// Add new client to database
        /// </summary>
        /// <param name="client">ClientModel object</param>
        public static void AddClient(ClientModel client)
        {
            string query = @"INSERT INTO bank.klient (imie, nazwisko, pesel)
                             VALUES (:ClientName, :ClientSurname, :ClientPesel)
                             RETURNING id_klienta";

            NpgsqlConnection connection = DatabaseConnection.GetInstance().Connection;

            try
            {
                using (NpgsqlTransaction transaction = connection.BeginTransaction())
                {
                    using (NpgsqlCommand cmd = new NpgsqlCommand(query, connection, transaction))
                    {
                        cmd.Parameters.Add(new NpgsqlParameter("ClientName", client.FirstName));
                        cmd.Parameters.Add(new NpgsqlParameter("ClientSurname", client.Surname));
                        cmd.Parameters.Add(new NpgsqlParameter("ClientPesel", client.Pesel));

                        var executeScalar = cmd.ExecuteScalar();
                        if (executeScalar != null)
                        {
                            int clientId = (int)executeScalar;

                            foreach (AddressModel address in client.Addresses)
                            {
                                AddressDao.AddAddress(address, clientId, transaction);
                            }

                            foreach (PhoneModel phone in client.Phones)
                            {
                                PhoneDao.AddPhone(phone, clientId, transaction);
                            }
                        }
                    }
                    transaction.Commit();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        /// <summary>
        /// Edit specified client in database
        /// <param name="client">ClientModel object</param>
        /// </summary>
        public static void EditClient(ClientModel client)
        {
            string query = @"UPDATE bank.klient 
                             SET imie = :ClientName, nazwisko = :ClientSurname, pesel = :ClientPesel
                             WHERE id_klienta = :ClientId";

            NpgsqlConnection connection = DatabaseConnection.GetInstance().Connection;
            try
            {
                using (NpgsqlTransaction transaction = connection.BeginTransaction())
                {
                    using (NpgsqlCommand cmd = new NpgsqlCommand(query, connection, transaction))
                    {
                        cmd.Parameters.Add(new NpgsqlParameter("ClientId", client.ClientId));
                        cmd.Parameters.Add(new NpgsqlParameter("ClientName", client.FirstName));
                        cmd.Parameters.Add(new NpgsqlParameter("ClientSurname", client.Surname));
                        cmd.Parameters.Add(new NpgsqlParameter("ClientPesel", client.Pesel));
                        cmd.ExecuteNonQuery();

                        foreach (AddressModel address in client.Addresses)
                        {
                            AddressDao.EditAddress(address, transaction);
                        }

                        foreach (PhoneModel phone in client.Phones)
                        {
                            PhoneDao.EditPhone(phone, transaction);
                        }
                    }
                    transaction.Commit();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}