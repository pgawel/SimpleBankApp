﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using Npgsql;

namespace SimpleBankApp.DAO
{
    public class DatabaseConnection
    {
        public NpgsqlConnection Connection { get; private set; }

        private static DatabaseConnection _dbConnectionInstance;

        /// <summary>
        /// Creates instance of DatabaseConnection class.
        /// </summary>
        /// <returns></returns>
        public static DatabaseConnection GetInstance()
        {
            if (_dbConnectionInstance == null)
                _dbConnectionInstance = new DatabaseConnection();

            return _dbConnectionInstance;
        }

        /// <summary>
        /// Private constructor
        /// </summary>
        private DatabaseConnection()
        {
            string connectionString = GetConnectionStringFromConfig();

            try
            {
                Connection = new NpgsqlConnection(connectionString);
                Connection.Open();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        /// <summary>
        /// Retrieve connection string from config file.
        /// </summary>
        /// <returns>Connection string</returns>
        protected string GetConnectionStringFromConfig()
        {
            try
            {
                return ConfigurationManager.ConnectionStrings["BankConnection"].ConnectionString;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return "";
            }
        }

        #region Dispose instance
        // flag - defines if 'Dispose' has been already called
        private bool _disposed;

        protected void Dispose(bool shallDispose)
        {
            if (_disposed)
                return;

            try
            {
                if (shallDispose)
                {
                    Connection?.Dispose();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            _disposed = true;
        }
        #endregion

        /// <summary>
        /// Closes connection with database when deploying class destructor.
        /// </summary>
        ~DatabaseConnection()
        {
            if (Connection.State != ConnectionState.Open)
                Connection?.Close();
            Dispose(false);
        }
    }
}