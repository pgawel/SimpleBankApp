﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Npgsql;
using SimpleBankApp.Models;

namespace SimpleBankApp.DAO
{
    public class AddressDao
    {
        /// <summary>
        /// Get specified address from database
        /// </summary>
        /// <returns>List of all addreses for given client</returns>
        public static AddressModel GetAddress(int addressId)
        {
            AddressModel address = new AddressModel();

            string query = @"SELECT * 
                             FROM bank.adres a 
                             WHERE a.id_adres = :AddressId";

            NpgsqlConnection connection = DatabaseConnection.GetInstance().Connection;

            // get data
            using (var cmd = new NpgsqlCommand(query, connection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("AddressId", addressId));
                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                        address = new AddressModel(reader);
            }

            return address;
        }

        /// <summary>
        /// Get addresses for specified client from database
        /// </summary>
        /// <returns>List of all addreses for given client</returns>
        public static List<AddressModel> GetAddressesList(int clientId)
        {
            List<AddressModel> addresses = new List<AddressModel>();

            string query = @"SELECT * 
                             FROM bank.adres a 
                             WHERE a.id_klienta = :ClientId";

            NpgsqlConnection connection = DatabaseConnection.GetInstance().Connection;

            // get data
            using (var cmd = new NpgsqlCommand(query, connection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("ClientId", clientId));
                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                        addresses.Add(new AddressModel(reader));
            }

            return addresses;
        }

        /// <summary>
        /// Add new address to database for specified client
        /// </summary>
        /// <param name="address">AddressModel object</param>
        /// <param name="clientId">Client's id</param>
        /// <param name="transaction"></param>
        public static void AddAddress(AddressModel address, int clientId, NpgsqlTransaction transaction)
        {
            string query = @"INSERT INTO bank.adres (id_klienta, ulica, nr_domu, nr_lokalu, kod_pocztowy, poczta, miasto)
                             VALUES (:ClientId, :Street, :HouseNum, :ApartmentNum, :PostalCode, :Post, :City)";

            NpgsqlConnection connection = DatabaseConnection.GetInstance().Connection;

            using (NpgsqlCommand cmd = new NpgsqlCommand(query, connection, transaction))
            {
                cmd.Parameters.Add(new NpgsqlParameter("ClientId", clientId));
                cmd.Parameters.Add(new NpgsqlParameter("AddressId", address.AddressId));
                cmd.Parameters.Add(new NpgsqlParameter("Street", address.Street));
                cmd.Parameters.Add(new NpgsqlParameter("HouseNum", address.HouseNum));
                cmd.Parameters.Add(new NpgsqlParameter("ApartmentNum", address.ApartmentNum));
                cmd.Parameters.Add(new NpgsqlParameter("PostalCode", address.PostalCode));
                cmd.Parameters.Add(new NpgsqlParameter("Post", address.Post));
                cmd.Parameters.Add(new NpgsqlParameter("City", address.City));
                cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Add new adress to database
        /// </summary>
        /// <param name="address">AddressModel object</param>
        /// <param name="clientId">Client's id</param>
        public static void AddAddress(AddressModel address, int clientId)
        {
            NpgsqlConnection connection = DatabaseConnection.GetInstance().Connection;
            try
            {
                using (NpgsqlTransaction transaction = connection.BeginTransaction())
                {
                    AddAddress(address, clientId, transaction);
                    transaction.Commit();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        /// <summary>
        /// Edit specified address in database
        /// </summary>
        /// <param name="address">AddressModel object</param>
        /// <param name="transaction"></param>
        public static void EditAddress(AddressModel address, NpgsqlTransaction transaction)
        {
            string query = @"UPDATE bank.adres 
                             SET ulica = :Street, nr_domu = :HouseNum, nr_lokalu = :ApartmentNum, kod_pocztowy = :PostalCode, poczta = :Post, miasto = :City
                             WHERE id_adres = :AddressId";

            NpgsqlConnection connection = DatabaseConnection.GetInstance().Connection;

            using (NpgsqlCommand cmd = new NpgsqlCommand(query, connection, transaction))
            {
                cmd.Parameters.Add(new NpgsqlParameter("AddressId", address.AddressId));
                cmd.Parameters.Add(new NpgsqlParameter("Street", address.Street));
                cmd.Parameters.Add(new NpgsqlParameter("HouseNum", address.HouseNum));
                cmd.Parameters.Add(new NpgsqlParameter("ApartmentNum", address.ApartmentNum));
                cmd.Parameters.Add(new NpgsqlParameter("PostalCode", address.PostalCode));
                cmd.Parameters.Add(new NpgsqlParameter("Post", address.Post));
                cmd.Parameters.Add(new NpgsqlParameter("City", address.City));
                cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Edit specified address in database
        /// </summary>
        /// <param name="address">AddressModel object</param>
        public static void EditAddress(AddressModel address)
        {
            NpgsqlConnection connection = DatabaseConnection.GetInstance().Connection;
            try
            {
                using (NpgsqlTransaction transaction = connection.BeginTransaction())
                {
                    EditAddress(address, transaction);
                    transaction.Commit();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        /// <summary>
        /// Remove address from database
        /// </summary>
        /// <param name="addressId">Address's id</param>
        public static void RemoveAddress(int addressId)
        {
            string query = @"DELETE FROM bank.adres
                             WHERE id_adres = :AddressId";

            NpgsqlConnection connection = DatabaseConnection.GetInstance().Connection;

            NpgsqlCommand cmd = new NpgsqlCommand(query, connection);
            cmd.Parameters.Add(new NpgsqlParameter("AddressId", addressId));
            cmd.ExecuteNonQuery();
        }
    }
}