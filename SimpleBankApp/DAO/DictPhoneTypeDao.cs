﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Npgsql;
using SimpleBankApp.Models;

namespace SimpleBankApp.DAO
{
    public class DictPhoneTypeDao
    {
        /// <summary>
        /// Get all phone types from database
        /// </summary>
        /// <returns>List of all phone types</returns>
        public static List<DictPhoneTypeModel> GetPhoneTypes()
        {
            List<DictPhoneTypeModel> items = new List<DictPhoneTypeModel>();

            string query = @"SELECT *
                             FROM bank.sl_rodzajtelefonu rt";

            NpgsqlConnection connection = DatabaseConnection.GetInstance().Connection;

            // read data
            using (var cmd = new NpgsqlCommand(query, connection))
            {
                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                        items.Add(new DictPhoneTypeModel(reader));
            }

            return items;
        }

        /// <summary>
        /// Get phone type id by name
        /// </summary>
        /// <param name="phoneTypeName">Name of a phone type</param>
        /// <returns>Phone type id</returns>
        public static int GetPhoneTypeId(string phoneTypeName)
        {
            List<DictPhoneTypeModel> items = GetPhoneTypes();

            foreach (DictPhoneTypeModel item in items)
            {
                if (item.PhoneTypeName == phoneTypeName)
                    return item.PhoneTypeId;
            }

            return -1;
        }
    }
}