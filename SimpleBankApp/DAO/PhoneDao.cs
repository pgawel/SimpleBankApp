﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Npgsql;
using SimpleBankApp.Models;

namespace SimpleBankApp.DAO
{
    public class PhoneDao
    {
        /// <summary>
        /// Get phones info for specified client from database
        /// </summary>
        /// <param name="clientId">Client's id</param>
        /// <returns>List of all phones for given client</returns>
        public static List<PhoneModel> GetPhonesList(int clientId)
        {
            List<PhoneModel> items = new List<PhoneModel>();

            string query = @"SELECT * 
                             FROM bank.telefon t 
                             JOIN bank.sl_rodzajtelefonu rt ON t.id_rodzajtelefonu = rt.id_rodzajtelefonu
                             WHERE t.id_klienta = :ClientId";

            NpgsqlConnection connection = DatabaseConnection.GetInstance().Connection;
            using (var cmd = new NpgsqlCommand(query, connection))
            {
                cmd.Parameters.Add(new NpgsqlParameter("ClientId", clientId));
                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                        items.Add(new PhoneModel(reader));
            }

            return items;
        }

        /// <summary>
        /// Add new phone to database for specified client
        /// </summary>
        /// <param name="phone">PhoneModel object</param>
        /// <param name="clientId">Client's id</param>
        /// <param name="transaction"></param>
        public static void AddPhone(PhoneModel phone, int clientId, NpgsqlTransaction transaction)
        {
            string query = @"INSERT INTO bank.telefon (id_klienta, id_rodzajtelefonu, nr_telefonu)
                             VALUES (:ClientId, :PhoneTypeId, :PhoneNumber)";

            NpgsqlConnection connection = DatabaseConnection.GetInstance().Connection;
            using (NpgsqlCommand cmd = new NpgsqlCommand(query, connection, transaction))
            {
                int phoneTypeId = DictPhoneTypeDao.GetPhoneTypeId(phone.PhoneType.PhoneTypeName);

                cmd.Parameters.Add(new NpgsqlParameter("ClientId", clientId));
                cmd.Parameters.Add(new NpgsqlParameter("PhoneTypeId", phoneTypeId));
                cmd.Parameters.Add(new NpgsqlParameter("PhoneNumber", phone.PhoneNumber));
                cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Edit specified phone in database
        /// </summary>
        public static void EditPhone(PhoneModel phone, NpgsqlTransaction transaction)
        {
            string query = @"UPDATE bank.telefon 
                             SET nr_telefonu = :PhoneNumber, id_rodzajtelefonu = :PhoneTypeId
                             WHERE id_telefonu = :PhoneId";

            NpgsqlConnection connection = DatabaseConnection.GetInstance().Connection;

            using (NpgsqlCommand cmd = new NpgsqlCommand(query, connection, transaction))
            {
                int phoneTypeId = DictPhoneTypeDao.GetPhoneTypeId(phone.PhoneType.PhoneTypeName);

                cmd.Parameters.Add(new NpgsqlParameter("PhoneId", phone.PhoneId));
                cmd.Parameters.Add(new NpgsqlParameter("PhoneNumber", phone.PhoneNumber));
                cmd.Parameters.Add(new NpgsqlParameter("PhoneTypeId", phoneTypeId));
                cmd.ExecuteNonQuery();
            }
        }
    }
}