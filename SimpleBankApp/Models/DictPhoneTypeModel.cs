﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Npgsql;

namespace SimpleBankApp.Models
{
    public class DictPhoneTypeModel
    {
        public int PhoneTypeId { get; set; }

        [Required, DisplayName("Phone type")]
        public string PhoneTypeName { get; set; }

        public DictPhoneTypeModel() { }

        public DictPhoneTypeModel(NpgsqlDataReader reader)
        {
            PhoneTypeId = (int)reader["id_rodzajtelefonu"];
            PhoneTypeName = reader["nazwa_rodzajtelefonu"].ToString();
        }
    }
}