﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Npgsql;

namespace SimpleBankApp.Models
{
    public class DictCurrencyModel
    {
        public int CurrencyId { get; private set; }

        [Required, DisplayName("Currency name")]
        public string CurrencyName { get; set; }

        [Required, DisplayName("Currency name (short)")]
        public string CurrencyShort { get; set; }

        public DictCurrencyModel(NpgsqlDataReader reader)
        {
            CurrencyId = (int)reader["id_waluty"];
            CurrencyName = reader["nazwa_waluty"].ToString();
            CurrencyShort = reader["skrot_waluty"].ToString();
        }
    }
}