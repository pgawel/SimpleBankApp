﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Npgsql;

namespace SimpleBankApp.Models
{
    public class TransactionModel
    {
        [DisplayName("Transaction id")]
        public int TransactionId { get; private set; }

        [DisplayName("Account id")]
        public int AccountId { get; private set; }

        [Required, DisplayName("Transaction date")]
        public string TransactionDate { get; set; }

        [DisplayName("Transaction type")]
        public DictTransactionType TransactionType { get; set; }

        public double Amount { get; set; }

        [Required, DisplayName("Related account")]
        public string RelatedAccountNumber { get; set; }

        public string Description { get; set; }

        public TransactionModel(NpgsqlDataReader reader)
        {
            TransactionId = (int)reader["id_operacji"];
            AccountId = (int)reader["id_rachunku"];
            TransactionDate = reader["data_operacji"].ToString();
            TransactionType = new DictTransactionType(reader);
            Amount = reader.IsDBNull(reader.GetOrdinal("kwota_operacji")) ? 0.0: Convert.ToDouble(reader["kwota_operacji"]);
            RelatedAccountNumber = reader["rachunek_powiazany"].ToString();
            Description = reader["opis"].ToString();
        }
    }
}