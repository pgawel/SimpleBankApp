﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Npgsql;

namespace SimpleBankApp.Models
{
    public class DictProductTypeModel
    {
        public int ProductTypeId { get; private set; }

        [Required, DisplayName("Product type")]
        public string ProductTypeName { get; set; }

        [Required, DisplayName("Product type (short)")]
        public string ProductTypeShort { get; set; }

        public DictProductTypeModel(NpgsqlDataReader reader)
        {
            ProductTypeId = (int)reader["id_rodzajproduktu"];
            ProductTypeName = reader["nazwa_rodzajproduktu"].ToString();
            ProductTypeShort = reader["skrot_rodzajproduktu"].ToString();
        }
    }
}