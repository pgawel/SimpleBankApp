﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Npgsql;

namespace SimpleBankApp.Models
{
    public class DictTransactionType
    {
        public int TransactionTypeId { get; private set; }

        [Required, DisplayName("Transaction type")]
        public string TransactionTypeName { get; set; }

        public DictTransactionType(NpgsqlDataReader reader)
        {
            TransactionTypeId = (int)reader["id_rodzajoperacji"];
            TransactionTypeName = reader["nazwa_rodzajoperacji"].ToString();
        }
    }
}