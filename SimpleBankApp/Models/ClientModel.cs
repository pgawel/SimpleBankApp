﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Npgsql;
using SimpleBankApp.DAO;

namespace SimpleBankApp.Models
{
    public class ClientModel
    {
        [DisplayName("Client id")]
        public int ClientId { get; set; }

        [Required, DisplayName("First name")]
        public string FirstName { get; set; }

        [Required, DisplayName("Surname")]
        public string Surname { get; set; }

        [RegularExpression(@"^\d{11}$", ErrorMessage = "Not a valid PESEL number.")]
        [Required, DisplayName("Pesel")]
        public long Pesel { get; set; }

        [DisplayName("Address")]
        public List<AddressModel> Addresses { get; set; }

        [DisplayName("Phone")]
        public List<PhoneModel> Phones { get; set; }

        public List<ClientsAccountModel> Accounts { get; set; }

        [Required]
        public DictRoleModel Role { get; set; }

        public ClientModel() { }

        public ClientModel(NpgsqlDataReader reader)
        {
            ClientId = (int)reader["id_klienta"];
            FirstName = reader["imie"].ToString();
            Surname = reader["nazwisko"].ToString();
            Pesel = (long)reader["pesel"];
        }
    }
}