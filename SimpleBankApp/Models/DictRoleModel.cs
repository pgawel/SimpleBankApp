﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Npgsql;

namespace SimpleBankApp.Models
{
    public class DictRoleModel
    {
        public int RoleId { get; private set; }

        public string RoleName { get; set; }

        public DictRoleModel(NpgsqlDataReader reader)
        {
            RoleId = (int) reader["id_roli"];
            RoleName = reader["nazwa_roli"].ToString();
        }
    }
}