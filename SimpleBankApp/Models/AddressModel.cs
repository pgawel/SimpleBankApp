﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Npgsql;

namespace SimpleBankApp.Models
{
    public class AddressModel
    {
        [DisplayName("Address id")]
        public int AddressId { get; set; }

        [DisplayName("Client id")]
        public int ClientId { get; set; }

        [Required]
        public string Street { get; set; }

        [Required, DisplayName("House number")]
        public string HouseNum { get; set; }

        [DisplayName("Apartment number")]
        public int ApartmentNum { get; set; }

        [Required, DisplayName("Postal code")]
        [RegularExpression(@"^([0-9]{2})-([0-9]{3})$", ErrorMessage = "Not a valid postal code (Pattern: 00-000).")]
        public string PostalCode { get; set; }

        [Required]
        public string Post { get; set; }

        [Required]
        public string City { get; set; }

        public AddressModel() { }

        public AddressModel(NpgsqlDataReader reader)
        {
            AddressId = (int)reader["id_adres"];
            ClientId = (int)reader["id_klienta"];
            Street = reader["ulica"].ToString();
            HouseNum = reader["nr_domu"].ToString();
            ApartmentNum = reader.IsDBNull(reader.GetOrdinal("nr_lokalu")) ? 0 : (int)reader["nr_lokalu"];
            PostalCode = reader["kod_pocztowy"].ToString();
            Post = reader["poczta"].ToString();
            City = reader["miasto"].ToString();
        }
    }
}