﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Npgsql;

namespace SimpleBankApp.Models
{
    public class PhoneModel
    {
        [DisplayName("Phone id")]
        public int PhoneId { get; set; }

        [DisplayName("Client id")]
        public int ClientId { get; set; }

        [DisplayName("Phone type")]
        public DictPhoneTypeModel PhoneType { get; set; }

        [Required, DisplayName("Phone number")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^([0-9]{3})?[-. ]?([0-9]{3})[-. ]?([0-9]{3})$", ErrorMessage = "Not a valid phone number.")]
        public string PhoneNumber { get; set; }

        public PhoneModel() { }

        public PhoneModel(NpgsqlDataReader reader)
        {
            PhoneId = (int)reader["id_telefonu"];
            ClientId = (int)reader["id_klienta"];
            PhoneType = new DictPhoneTypeModel(reader);
            PhoneNumber = reader["nr_telefonu"].ToString();
        }
    }
}