﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Npgsql;

namespace SimpleBankApp.Models
{
    public class ClientsAccountModel
    {
        [DisplayName("Account id")]
        public int AccountId { get; private set; }

        [Required, DisplayName("Product type")]
        public DictProductTypeModel ProductType { get; set; }

        [Required, DisplayName("Opening date")]
        public DateTime OpeningDate { get; set; }

        [Required]
        public DictCurrencyModel Currency { get; set; }

        [Required, DisplayName("Closure date")]
        public DateTime ClosureDate { get; set; }

        [Required, DisplayName("Number")]
        public string AccountNumber { get; set; }

        [Required, DisplayName("Balance")]
        public double AccountBalance { get; set; }

        [Required, DisplayName("Is active?")]
        public bool IsActive { get; set; }

        public List<TransactionModel> Transactions { get; set; }
        
        public List<ClientModel> Clients { get; set; }

        [Required]
        public DictRoleModel Role { get; set; }

        public ClientsAccountModel() { }

        public ClientsAccountModel(NpgsqlDataReader reader)
        {
            AccountId = (int) reader["id_rachunku"];
            ProductType = new DictProductTypeModel(reader);
            OpeningDate = (DateTime)reader["data_otwarcia"];
            Currency = new DictCurrencyModel(reader);
            ClosureDate = reader.IsDBNull(reader.GetOrdinal("data_zamkniecia")) ? new DateTime() : (DateTime)reader["data_zamkniecia"];
            AccountNumber = reader["nr_rachunku"].ToString();
            AccountBalance = Convert.ToDouble(reader["saldo"]);
            IsActive = (bool) reader["czy_aktywny"];
        }
    }
}