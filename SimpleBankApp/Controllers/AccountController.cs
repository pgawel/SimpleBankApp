﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SimpleBankApp.DAO;
using SimpleBankApp.Models;

namespace SimpleBankApp.Controllers
{
    public class AccountController : Controller
    {
        /// <summary>
        /// Display all accounts
        /// </summary>
        /// <returns>Index view of AccountController</returns>
        public ActionResult Index()
        {
            List<ClientsAccountModel> accounts = ClientsAccountDao.GetAccountsList();

            return View(accounts);
        }

        /// <summary>
        /// Display account's details view
        /// </summary>
        /// <param name="accountId">Accounts's id</param>
        /// <returns>Accounts's details view</returns>
        public ActionResult Details(int accountId)
        {
            ClientsAccountModel account = ClientsAccountDao.GetAccount(accountId);

            return account != null ? View(account) : View();
        }

        /// <summary>
        /// Display transactions for given account id
        /// </summary>
        /// <param name="accountId">Account id</param>
        /// <returns>Transactions view</returns>
        public ActionResult Transactions(int accountId)
        {
            ClientsAccountModel account = ClientsAccountDao.GetAccount(accountId);

            return account != null ? View(account) : View();
        }
    }
}