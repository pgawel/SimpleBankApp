﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SimpleBankApp.DAO;
using SimpleBankApp.Models;

namespace SimpleBankApp.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Simple bank application which process data about clients, bank accounts, transactions etc. from database.";

            return View();
        }
    }
}