﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SimpleBankApp.DAO;
using SimpleBankApp.Models;

namespace SimpleBankApp.Controllers
{
    public class ClientController : Controller
    {
        /// <summary>
        /// Display all clients
        /// </summary>
        /// <returns>Index view of ClientController</returns>
        public ActionResult Index()
        {
            List<ClientModel> clients = ClientDao.GetClientsList();

            return View(clients);
        }

        /// <summary>
        /// Display client's details view
        /// </summary>
        /// <param name="clientId">Client's id</param>
        /// <returns>Client's details view</returns>
        public ActionResult Details(int clientId)
        {
            ClientModel client = ClientDao.GetClient(clientId);

            return client != null ? View(client) : View();
        }

        /// <summary>
        /// Display 'AddNewClient' view
        /// </summary>
        /// <returns>'AddNewClient' view</returns>
        public ActionResult AddNewClient()
        {
            List<DictPhoneTypeModel> phoneTypes = DictPhoneTypeDao.GetPhoneTypes();
            ViewBag.PhoneTypeName = new SelectList(phoneTypes, "PhoneTypeName", "PhoneTypeName");

            return View();
        }

        /// <summary>
        /// Handle 'AddNewClient' form
        /// </summary>
        /// <param name="client">Client's model</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddNewClient(ClientModel client)
        {
            ClientDao.AddClient(client);
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Display 'EditClient' view
        /// </summary>
        /// <param name="clientId">Client's id</param>
        /// <returns>'EditClient' view</returns>
        public ActionResult EditClient(int clientId)
        {
            ClientModel client = ClientDao.GetClient(clientId);

            if (client == null)
            {
                return HttpNotFound();
            }

            List<DictPhoneTypeModel> phoneTypes = DictPhoneTypeDao.GetPhoneTypes();
            ViewBag.PhoneTypeName = new SelectList(phoneTypes, "PhoneTypeName", "PhoneTypeName", client.Phones[0].PhoneType.PhoneTypeName);

            return View(client);
        }

        /// <summary>
        /// Handle 'EditClient' form
        /// </summary>
        /// <param name="client">Client's model</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult EditClient(ClientModel client)
        {
            ClientDao.EditClient(client);
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Display 'AddNewAddress' view
        /// </summary>
        /// <returns>'AddNewAddress' view</returns>
        public ActionResult AddNewAddress(int clientId)
        {
            AddressModel address = new AddressModel();
            address.ClientId = clientId;

            return View(address);
        }

        /// <summary>
        /// Handle 'AddNewAddress' form
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddNewAddress(AddressModel address, int clientId)
        {
            if (ModelState.IsValid)
            {
                AddressDao.AddAddress(address, clientId);

                return RedirectToAction("Details", "Client", new { clientId = address.ClientId });
            }
            return View(address);
        }

        /// <summary>
        /// Display 'EditAddress' view
        /// </summary>
        /// <param name="addressId">Address's id</param>
        /// <returns>'EditAddress' view</returns>
        public ActionResult EditAddress(int addressId)
        {
            AddressModel address = AddressDao.GetAddress(addressId);

            if (address == null)
            {
                return HttpNotFound();
            }

            return View(address);
        }

        /// <summary>
        /// Handle 'EditAddress' form
        /// </summary>
        /// <param name="address">Address's model</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult EditAddress(AddressModel address)
        {
            if (ModelState.IsValid)
            {
                AddressDao.EditAddress(address);

                return RedirectToAction("Details", "Client", new { clientId = address.ClientId });
            }

            return View(address);
        }

        /// <summary>
        /// Display 'RemoveAddress' view
        /// </summary>
        /// <param name="addressId">Address's id</param>
        /// <returns>'RemoveAddress' view</returns>
        public ActionResult RemoveAddress(int addressId)
        {
            AddressModel address = AddressDao.GetAddress(addressId);

            if (address == null)
            {
                return HttpNotFound();
            }

            return View(address);
        }

        /// <summary>
        /// Remove address from database
        /// </summary>
        /// <returns>Redirection to main page</returns>
        [HttpPost]
        public ActionResult RemoveAddress(int addressId, FormCollection collection)
        {
            AddressModel address = AddressDao.GetAddress(addressId);
            AddressDao.RemoveAddress(addressId);

            return RedirectToAction("Details", "Client", new { clientId = address.ClientId });
        }
    }
}